---
layout: post
title: "HomeKit Integration"
comments: true
share: false
description: "How I enabled Siri to control my computer"
keywords: "homekit, siri"
---
I've started to become more interested in the IoT space. I think there are many
applications of smart technology that will enrich our lives in the future.

### Overview of HomeKit
HomeKit is several things. It is:

* A database
* A protocol
* An app (starting in iOS 10)

Internally, HomeKit is a database that represents your devices, rooms,
and scenes (macros). HomeKit uses iCloud Keychain to syncrhonize cryptographic
keys which are used to secure the platform. When you are away from your home,
a command to control a device gets sent to Apple, where it gets relayed to either
an Apple TV or an iPad at home. The Apple TV or iPad then relays the command to
the HomeKit device via the HomeKit protocol. Starting in iOS 10, Apple has created
a 'Home' app which allows you to control and manage HomeKit devices without having
to use Siri (pre iOS 10).

### My Home
Currently I have several Hue lightbulbs and an Ecobee thermostat which are
HomeKit compatible. One thing that I wanted to bring into the mix was my
computer as I wanted a way to have it automatically shutoff when it's bedtime.

### HomeBridge
[HomeBridge](https://github.com/nfarina/homebridge) is a NodeJS server which
runs on my internal network and acts as a compatible HomeKit device for controlling
non HomeKit compatible devices. Currently, it is running on my Raspberry Pi.
I'm also using the
[HomeBridge-WOL](https://github.com/AlexGustafsson/homebridge-wol) plugin which
is used to turn the computer on and off.

The following is my config file for HomeBridge

``` json
{
    "bridge": {
        "name": "Homebridge",
        "username": "<mac-of-raspberrypi-here>",
        "port": 51826,
        "pin": "123-45-678"
    },

    "description": "Homebridge",

    "accessories": [
        {
            "accessory": "Computer",
            "name": "Computer",
            "mac": "<mac-of-computer-here>",
            "ip": "10.10.0.3",
            "pingInterval": 45,
            "wakeGraceTime": 90,
            "shutdownGraceTime": 15,
            "shutdownCommand": "/root/winexe-waf/source/build/winexe -A /root/auth.txt //<computer-name-here> 'psshutdown -c -d -m \"HomeKit triggered standby\" -t 0'"
        }
    ]
}
```

First, waking the computer is the easy part. When the HomeKit command is received
the HomeBridge-WOL package will create a
[WOL Magic Packet](https://wiki.wireshark.org/WakeOnLan) to wake up the computer.
What was more challenging was getting the computer to turn off. I didn't want to fully
shutdown the computer, because remote start would work as I would have to type
my boot password for BitLocker. This ruled out running `net rpc shutdown` or using
the built in `shutdown.exe` command as it only supports to shutdown, or hibernate
the computer, without any standby options. I settled for using a program called
[WinEXE](https://sourceforge.net/projects/winexe) which uses Samba to execute
commands. I also downloaded the
[PsShutdown](https://technet.microsoft.com/en-us/sysinternals/psshutdown.aspx)
utility which is basically what `shutdown.exe` should have been. After all that,
I used the winexe command on the Raspberry Pi to connect to my computer and have
it run psshutdown with the `-d` flag, which suspends the computer rather than
shutting it down.
