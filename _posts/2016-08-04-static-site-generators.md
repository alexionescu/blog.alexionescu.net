---
layout: post
title: "Benefits of Static Site Generators"
comments: true
share: false
description: "How I use a Static Site Generator"
keywords: "static, ssg, jekyll"
---
When you interact with a website there are a lot of things that happen behind
the scenes. For example, when you navigate to Reddit, a front-end load balancer
dispatches your request to a server that generates the page for you. Different
users are subscribed to different subreddits, so in general everyone will see
different content. This backend server is probably talking to several databases
in order to be able to construct the page. These types of websites are called
*dynamic* as the web server does not simply serve a static page to the end user.

### Security Flaws with Dynamic Websites
One of the biggest issues with dynamic websites is that they trust the end user
too much. Insufficient input sanitization can lead to
[SQL Injections](https://en.wikipedia.org/wiki/SQL_injection) or
[XSS](https://en.wikipedia.org/wiki/Cross-site_scripting). This is where
static site generators can come into play!

### What is an SSG?
A static site generator (SSG) is a program that takes some input files and creates
a static website out of them. The important thing to note is that the website does
not *appear* to be static. In fact, this website is built using
[Jekyll](https://jekyllrb.com/), a blog-aware SSG. Since it's blog aware it can
generate what appears to be blog posts, complete with permalink support, and
pagination. You can see the source code for my website
[here](https://gitlab.com/alexionescu/alexionescu.gitlab.io). So why are SSGs good?
Well, they avoid a lot of the common pitfalls with dynamic websites. Since
every page is static, your attack surface is dramatically reduced. There are no
databases or user input to exploit. One drawback to an SSG-generated site is that
the generator needs to be re-ran for every update to the site. I wanted to share
how this site is run in order to make it as simple as possible to host an SSG
website.

### Continuous Integration
Because the site has to be regenerated by Jekyll with every update this is
the perfect use case for
[Continuous Integration](https://www.thoughtworks.com/continuous-integration).
Put simply, continuous integration is the usage of an automated tool to
run every update in order to check code for errors, and create an artifact
if there are no errors. In this case, the artifact of the Jekyll tool would be
the HTML/CSS that make up the website.
[Gitlab](https://gitlab.com) offers free CI servers which are being run in
[DigitalOcean's](https://digitalocean.com) cloud environment. When code
is being pushed to the git repository Gitlab will take a look at the
.gitlab-ci.yml file in order to figure out what the CI server should do.

``` yaml
image: ruby:2.3

pages:
  script:
  - gem install jekyll
  - bundle install
  - bundle exec jekyll build -d public
  artifacts:
    paths:
    - public
  only:
    - master
```

First, the `image` key specifies to run the CI server using a Ruby 2.3 environment.
In this case, this means using a [Docker](https://docker.com) container with Ruby
2.3 installed. The `pages` key is the name of the job. It has a `script` containing
three commands. First Jekyll will be installed on the machine, then all other
prerequisites for the project, and finally the output will be built by Jekyll and
placed in a directory named `public`. The artifact for Gitlab pages is the public
folder and this only will get run when a push to the master branch is performed with Git.
Here is what the output looks like when I push to the repository.

{% include image.html url="https://i.imgur.com/YmTQt4b.png" description="Summary view of a CI build." %}

We can even view the logs for the CI build!

{% include image.html url="https://i.imgur.com/iZwSL5g.png" description="Logs that the CI build generated." %}

When it comes to security the Keep it Simple, Stupid (KISS) model is great, and SSGs
are a perfect example of the idea.
